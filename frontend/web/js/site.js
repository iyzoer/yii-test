$(function() {
    $('#calculate-form').submit(function(e) {
        let $form = $(this);
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize()
        }).done(function(res) {
            if (res.error) {
                $('.alert-danger').html(
                    '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>\n' +
                    '<h4><i class="icon fa fa-check"></i>Error!</h4>\n' +
                    '<p>' + res.error + '</p>'
                ).show()
            } else {
                $('.alert-success').html(
                    '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>\n' +
                    '<h4><i class="icon fa fa-check"></i>Success!</h4>\n' +
                    '<p>' + res.price + '</p>' +
                    '<p>' + res.info + '</p>'
                ).show()
            }
        }).fail(function(e) {
            console.log(e);
        });

        e.preventDefault();
        e.stopImmediatePropagation();
    });
});