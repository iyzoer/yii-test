<?php

namespace frontend\controllers;

use common\models\User;
use frontend\models\CalculateModel;
use yii\web\Response;

class SoapController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => 'mongosoft\soapserver\Action',
                'serviceOptions' => [
                    'disableWsdlMode' => false,
                    'enableCaching' => false
                ]
            ]
        ];
    }

    /**
     * @param string $city
     * @param string $name
     * @param string $date
     * @return mixed
     * @throws \SoapFault
     * @soap
     */
    public function Calculate($city, $name, $date)
    {
        $this->auth();

        $calculateModel = new CalculateModel();
        $calculateModel->city = $city;
        $calculateModel->name = $name;
        $calculateModel->date = $date;

        return $calculateModel->calculate();
    }

    /**
     * @throws \SoapFault
     */
    private function auth()
    {
        $username = \Yii::$app->getRequest()->getAuthCredentials()[0];
        $password = \Yii::$app->getRequest()->getAuthCredentials()[1];

        if (!$username || !$password) {
            throw new \SoapFault(Response::$httpStatuses[403], 403);
        }

        $user = User::findByUsername($username);

        if (!$user) {
            throw new \SoapFault(Response::$httpStatuses[403], 403);
        }

        if (!$user->validatePassword($password)) {
            throw new \SoapFault(Response::$httpStatuses[401], 401);
        }
    }
}
