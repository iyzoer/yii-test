<?php

namespace frontend\controllers;

use frontend\models\CalculateModel;
use yii\web\Controller;

class CalculateController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $model = new CalculateModel();

        if ($model->load(\Yii::$app->request->post())) {
            return $this->asJson($model->calculate());
        }

        return $this->render('index', [
            'model' => $model
        ]);
    }
}
