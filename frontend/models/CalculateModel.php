<?php

namespace frontend\models;

use yii\base\Model;

class CalculateModel extends Model
{
    public $city;
    public $name;
    public $date;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['date', 'validateDate'],
            ['date', 'date', 'format' => 'yyyy-mm-dd']
        ];
    }

    /**
     * Calculate
     *
     * @return array
     */
    public function calculate()
    {
        if ($this->validate()) {
            return ['price' => \rand(0, 5000), 'info' => 'Information message'];
        }

        return ['error' => 'The past date cannot be specified'];
    }

    /**
     * Validate input date
     *
     * @throws \Exception
     */
    public function validateDate()
    {
        $nowDate = new \DateTime(\date('Y-m-d'));
        $date = new\DateTime($this->date);

        if ($nowDate > $date) {
            $this->addError('date');
        }
    }
}
