<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Calculate';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'calculate-form']); ?>

            <?= $form->field($model, 'city') ?>

            <?= $form->field($model, 'name') ?>

            <?= $form->field($model, 'date') ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'calculate-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <div class="alert alert-success alert-dismissable" style="display: none">
    </div>
    <div class="alert alert-danger alert-dismissable" style="display: none">
    </div>
</div>