### Install

Run

`git clone https://iyzoer@bitbucket.org/iyzoer/yii-test.git`

`composer install`

`/path/to/php init`

### Usage

In your PHP file

```php
$client = new \SoapClient(
    'http://your-frontend-url/soap/index',
    [
        'login' => 'login',
        'password' => 'password'
    ]
);

try {
    $res = $client->__soapCall(
    'Calculate',
    [
        'city' => 'Moscow',
        'name' => 'Aleksandr',
        'date' => '2019-01-01'
        ]
    );

    var_dump($res);
} catch (\SoapFault $e) {
    var_dump($e->faultcode);
    var_dump($e->faultstring);
}
```